# 吉志龙
## 软件工程师

> 上海，男，1988年
>
> [zhilongji@gmail.com](zhilongji@gmail.com)
>
> [github](https://github.com/jizhilong)
>
> [个人博客](https://www.jianshu.com/u/28b243f8a082)
>
> (电话) Kzg2LTE4ODE3NTU0ODE0Cg==

---


### 教育背景

2011-2014
:   **硕士, 核科学与工程**; 上海交通大学

2007-2011
:   **学士, 物理学院**; 南京大学

---

### 工作经历

#### **上海乐言信息科技有限公司(2017.7-今):**

* infrastructure 工程师：SRE, docker, python, AI training pipeline, micro services

* 某核心业务项目技术负责人：java, groovy, 函数式编程, gRPC, 重构, 基于流量录制的自动化黑盒测试

* 工程效率 team leader：CI/CD, Python, Golang, Java, gRPC, maven, 基于代码生成的自动化回归测试, 类型安全的随机数据生成器

#### **上海携程信息科技有限公司(2014.4-2017.5):**

* 基于 OpenStack 的私有云平台：Python, OpenStack, 故障排查

* 基于 Mesos 的容器部署服务: Mesos, Python, Golang

---

### 技能

* 编程语言: 
  - 熟练使用 Java、Python、Golang 进行开发。学习和使用过 scheme，Haskell 等倡导函数式编程的语言。对使用函数式编程思想进行软件设计和开发有浓厚兴趣。

* Infrastructure
  -  **熟练使用 Docker**: 给社区提过 bug report 和 fix PR；实现过基于 Mesos 的容器化服务调度器；使用 Docker 的 entrypoint 机制实现过 pre-start hook；发布过一个 docker 镜像构建编排工具-dokcer-make
  -  **理解 vxlan、calico 等网络虚拟化方案**: 曾使用 vxlan 模拟携程的网络拓扑结构用于测试私有云中的网络相关组件

* 其他工具
  -  **理解maven**: 编写过 maven 插件；构建过基于 maven 的项目模板

  -  **深入理解 gitlab及 gitlab ci**: 开发过基于 gitlab comment event 的类 chatbot 自动化助手。

* 开源
  - 给 docker 提过 bug report([1](https://github.com/moby/moby/issues/23157), [2](https://github.com/moby/moby/issues/24671)) 和 [pull request](https://github.com/moby/moby/pull/24740)

  - [**docker-make**](https://github.com/ctripcloud/docker-make): 简化docker image build, tag, push 流程的命令行工具

  - [**docker-monitor-injector**](https://github.com/ctripcloud/docker-monitor-injector) 使用 `LD_PRELOAD` 劫持应用对C标准库函数的调用，使得 `top` 等命令能在容器内正确获取性能指标。

  - [**docker-wait**](https://github.com/jizhilong/docker-wait) 基于 docker 的 entrypoint 机制和 unix socket 实现容器的 pre-start hook。

  - [**js**](https://github.com/jizhilong/js) 方便健身打卡的 chatbot
