# JiZhilong
## Software Engineer

> Shanghai, Male, 1988
>
> [zhilongji@gmail.com](zhilongji@gmail.com)
>
> [github](https://github.com/jizhilong)
>
> [blog](https://www.jianshu.com/u/28b243f8a082)
>
> (Tel) Kzg2LTE4ODE3NTU0ODE0Cg==



### Education

2011-2014
:   **Master, School of Nuclear Science and Engineering**; Shanghai Jiaotong University

2007-2011
:   **Bachelor, Department of Physics**; Nanjing University


### Experience

#### **Shanghai Leyan Technology(2017.7-Now):**

* infrastructure engineer: SRE, docker, python, AI training pipeline, micro services

* core maintainer of leyan's dialogue engine: java, groovy, functional programming, gRPC, refactoring, automated black box test based on traffic recording

* leader of Engineer Effieciency team: CI/CD, Python, Golang, Java, gRPC, maven, automated regression test based on code generation, type-safe business data generator

#### **Ctrip(2014.4-2017.5):**

* maintainer of private cloud services based on OpenStack: Python, OpenStack, troubleshooting

* core maintainer of a container scheduler based on Mesos: Mesos, Python, Golang


### Skills

* Programming Language: 
  - master Java, Python, Golang
  - have experience with development in functional programming languages like scheme and Haskell.

* Infrastructure
  -  **Master Docker**: contributed bug report and PR to docker community; implemented a Mesos based container scheduler; implemented a pre-start hook for docker; opensourced a tool named docker-make to simplify docker image building pipeline
  -  **Understand virtual network stacks like vxlan, calico, etc.**: proposed a solution to simulate Ctrip's link-level network with vxlan.

* Other Tools
  -  **deep understanding of GitLab CI/CD**: developed a chatbot-like feature based on GitLab comment event and GitLab CI/CD

  -  **deep understanding of maven**: experience in maven plugin development

* Opensource 
  - docker: reported and fixed a bug of races between docker and udev on device symlink removal-https://github.com/moby/moby/issues/24671, https://github.com/moby/moby/pull/24740

  - [**docker-make**](https://github.com/ctripcloud/docker-make): a CLI tool to simplify the pipeline of building multiple related docker images from a git repo.

  - [**docker-monitor-injector**](https://github.com/ctripcloud/docker-monitor-injector): make commands like `top` normally by hijacking its call to libc via `LD_PRELOAD` mechanism.

  - [**docker-wait**](https://github.com/jizhilong/docker-wait): implement a pre-start hook for docker containers without any change to docker-engine.
